'use strict';

function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch

  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userInfo)
  };
  console.log(options)

  return fetch('/users', options)
  .then(function(response) {
    if (response.status === 201) {
      return response.json();
    } else {
      throw new Error(`Запрос завершился не успешно ${response.status} ${response.statusText}`);
    }
  })
  .then(function(response) {
    return response.id;
  })
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
 return fetch(`/users/${id}`)
  .then(function(response) {
    if (response.status === 200) {
      return response.json();
    } else {
      throw new Error(`Запрос завершился не успешно ${response.status} ${response.statusText}`);
    }
  })
}

function addUserXHR(userInfo) {
  return new Promise(function(resolve, reject) {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/users', true);
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onload = function() {
      if (this.status === 201) {
        resolve(this.response);
      } else {
        const error = new Error(`Запрос завершился не успешно ${this.status} ${this.statusText}`);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function() {
      reject(new Error("Network Error"));
    };
    
    xhr.send(JSON.stringify(userInfo)); 

  });
}


function getUserXHR(id) {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/users/${id}`, true);
    
    xhr.onload = function() {
      if (xhr.status === 200) {
        resolve(this.response.id);
      } else {
        const error = new Error(`Запрос завершился не успешно ${this.status} ${this.statusText}`);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function() {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });
}

function checkWork() {  
  addUserXHR({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

// checkWork();